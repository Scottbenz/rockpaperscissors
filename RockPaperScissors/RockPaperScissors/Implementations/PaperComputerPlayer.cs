﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.Enums;

namespace RockPaperScissors.Implementations
{
    public class PaperComputerPlayer : Player
    {
        public PaperComputerPlayer(string Name) : base(Name)
        {
        }

        public override Choice GetChoice()
        {
            return Choice.Paper;
        }
    }
}
