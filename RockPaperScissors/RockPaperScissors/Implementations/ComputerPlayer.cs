﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.Enums;

namespace RockPaperScissors.Implementations
{
    public class ComputerPlayer : Player
    {
        private static Random _randomGenerator = new Random();
        //private Random _randomGenerator;
        //Now can generate randomly for two computer players.

        public ComputerPlayer(string Name) : base(Name)
        {
            //_randomGenerator = new Random();
        }

        public override Choice GetChoice()
        {
            int i = _randomGenerator.Next(1, 4);

            return (Choice) i;
        }

        //What we need: A random generator to create output. 70% rock wins, 20% scissors, 10% paper.
    }
}
