﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.Enums;

namespace RockPaperScissors.Implementations
{
    public class WeightedComputerPlayer : Player
    {
        private static Random _randomGenerator = new Random();
        //private Random _randomGenerator;
        //Now can generate randomly for two computer players.

        public WeightedComputerPlayer(string Name) : base(Name)
        {
            //_randomGenerator = new Random();
        }

        public override Choice GetChoice()
        {
            int i = _randomGenerator.Next(1, 11);

            if (i <= 7)
            {
                return Choice.Rock;
            }
            else if (i == 8 || i == 9)
            {
                return Choice.Scissors;
            }
            else return Choice.Paper;

            //return (Choice)i;
        }
    }
}
