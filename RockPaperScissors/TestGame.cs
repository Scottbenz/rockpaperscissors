﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissors.Enums;
using RockPaperScissors.Implementations;

namespace RockPaperScissors
{
    public class TestGame
    {
        public Result PlayRound(Choice a)
        {
            //Player p1 = new HumanPlayer("Player 1");
            Player p2 = new PaperComputerPlayer("Player 2");

            MatchResult result = new MatchResult();
            result.Player1_Choice = a;
            result.Player2_Choice = p2.GetChoice();

            if (result.Player1_Choice == result.Player2_Choice)
            {
                //result.Match_Result = Result.Tie;
                return Result.Tie;
            }
            else if ((result.Player1_Choice == Choice.Rock && result.Player2_Choice == Choice.Scissors) ||
                     (result.Player1_Choice == Choice.Rock && result.Player2_Choice == Choice.Lizard) ||
                     (result.Player1_Choice == Choice.Paper && result.Player2_Choice == Choice.Rock) ||
                     (result.Player1_Choice == Choice.Paper && result.Player2_Choice == Choice.Spock) ||
                     (result.Player1_Choice == Choice.Scissors && result.Player2_Choice == Choice.Paper) ||
                     (result.Player1_Choice == Choice.Spock && result.Player2_Choice == Choice.Scissors) ||
                     (result.Player1_Choice == Choice.Spock && result.Player2_Choice == Choice.Rock) ||
                     (result.Player1_Choice == Choice.Lizard && result.Player2_Choice == Choice.Spock) ||
                     (result.Player1_Choice == Choice.Lizard && result.Player2_Choice == Choice.Paper))
            {
                //result.Match_Result = Result.Win;
                return Result.Win;
            }
            else
            {
                //result.Match_Result = Result.Loss;
                return Result.Loss;
            }
        }
    }
}
