﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RockPaperScissors;
using RockPaperScissors.Enums;

namespace RockPaperScissorTest
{
    public class Class1
    {

        [TestFixture]
        public class RPSTesting
        {
            //Test for Win
            [TestCase(Choice.Scissors, Result.Win)]
            [TestCase(Choice.Rock, Result.Loss)]
            [TestCase(Choice.Paper, Result.Tie)]
            public void AlwaysPaper(Choice choice, Result expectedResult)
            {
                TestGame blah = new TestGame();

                Result result = blah.PlayRound(choice);
                
                Assert.AreEqual(expectedResult, result);
            }
        }
    }
}
